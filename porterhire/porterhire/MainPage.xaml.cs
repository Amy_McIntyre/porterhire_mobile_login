﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace porterhire
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            LocalImage.Source = ImageSource.FromUri(new Uri("https://www.ruralco.org.nz/globalassets/images/suppliers/_default/porter-hire-logo.jpg?h=200&w=300&scale=canvas"));
        }

        private void login_Clicked(object sender, EventArgs e)
        {

        }
    }
}
